{
  description = "Trivial web server";
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };


  outputs = { self, nixpkgs, flake-utils}: flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system;};
        static = pkgs.stdenv.mkDerivation {
          name = "static";
          src = ./.;
          buildPhase = "";
          installPhase = "cp -r large-files $out";
        };
        server = pkgs.writeShellApplication {
              name = "toy-server";
              runtimeInputs = [pkgs.python311 static];
              text = ''
                  exec ${pkgs.python311}/bin/python -m http.server -d ${static} "$@"
              '';
          };
      in
    {
      packages = {
        default = server;
        static = static;
        server = server;
      };
      nixosModules = { config, lib, ... }: {
        options.lila.services.toy-server.enable =
          lib.mkEnableOption "enable Toy Server bot";
        options.lila.services.toy-server.port = lib.mkOption {
          type = lib.types.int;
          default = 8787;
          description = "The port the server will serve on.";
        };  

        config = lib.mkIf config.lila.services.toy-server.enable {
          users.groups.toy-server = { };

          users.users.toy-server = {
            createHome = true;
            isSystemUser = true;
            home = "/var/lib/toy-server";
            group = "toy-server";
          };

          systemd.services.toy-server = {
            wantedBy = [ "multi-user.target" ];
            environment.RUST_LOG = "tower_http=debug,info";
            serviceConfig = {
              User = "toy-server";
              Group = "toy-server";
              Restart = "always";
              WorkingDirectory = "/var/lib/toy-server";
              ExecStart = "${self.packages."${system}".server}/bin/toy-server ${builtins.toString config.lila.services.toy-server.port}";
            };
          };
        };
      };
    }
  );
}
